package com.service.employeedetails.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/api/v1")
@RestController
public class EmployeeController {	
	
	@GetMapping("/index")
	public String index() {
		return "Employee Details Service...!";
	}

}
